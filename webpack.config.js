const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: './src',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    libraryTarget: 'commonjs2'
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      }
    ]
  },
  externals: {
    '@atlaskit/avatar': '@atlaskit/avatar',
    '@atlaskit/button': '@atlaskit/button',
    '@atlaskit/dropdown-menu': '@atlaskit/dropdown-menu',
    '@atlaskit/icon/glyph/more': '@atlaskit/icon/glyph/more',
    'prop-types': 'prop-types',
    'react': 'react',
    'react-dom': 'react-dom'
  }
}
